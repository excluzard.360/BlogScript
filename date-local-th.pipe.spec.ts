import { DateLocalThPipe } from './date-local-th.pipe';

describe('DateLocalThPipe', () => {
  let pipe : DateLocalThPipe;

  var dateTest = new Date();
  dateTest.setDate(14);
  dateTest.setMonth(1);
  dateTest.setFullYear(2019);

  it('create an instance', () => {

    pipe = new DateLocalThPipe();
    expect(pipe).toBeTruthy();
  });

  /*full full-short  full-long short long numeric*/
  
  it('pip Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest);
    expect(filtered).toEqual('14/2/2562');
  });

  it('pip "numericxx" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "numericxx");
    console.log(filtered);
    expect(filtered).toEqual('14/2/2562');
  });

  it('pip ["numericxx", null] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["numericxx", null]);
    console.log(filtered);
    expect(filtered).toEqual('14/2/2562');
  });

  it('pip ["numericxx", "SSDd"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["numericxx", "SSDd"]);
    console.log(filtered);
    expect(filtered).toEqual('14/2/2562');
  });

  it('pip "full" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "full");
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });

  it('pip "full-long" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "full-long");
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });

  it('pip "full-short" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "full-short");
    console.log(filtered);
    expect(filtered).toEqual('พฤ. 14 ก.พ. 2562');
  });

  it('pip "full-short" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "full-short");
    console.log(filtered);
    expect(filtered).toEqual('พฤ. 14 ก.พ. 2562');
  });

  it('pip "short" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "short");
    console.log(filtered);
    expect(filtered).toEqual('14 ก.พ. 2562');
  });

  it('pip "numeric" Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, "numeric");
    console.log(filtered);
    expect(filtered).toEqual('14/2/2562');
  });


  // --------------


  it('pip ["full"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full"]);
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });

  it('pip ["full-long"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full-long"]);
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });

  it('pip ["full-short"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full-short"]);
    console.log(filtered);
    expect(filtered).toEqual('พฤ. 14 ก.พ. 2562');
  });

  it('pip ["full-short"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full-short"]);
    console.log(filtered);
    expect(filtered).toEqual('พฤ. 14 ก.พ. 2562');
  });

  it('pip ["short"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["short"]);
    console.log(filtered);
    expect(filtered).toEqual('14 ก.พ. 2562');
  });

  it('pip ["numeric"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["numeric"]);
    console.log(filtered);
    expect(filtered).toEqual('14/2/2562');
  });

// ------

  it('pip ["full"] Date : ' + dateTest.toUTCString(), () => {
    const filtered = pipe.transform(dateTest, ["full"]);
    console.log(filtered);
    expect(filtered).toEqual('วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562');
  });




});
