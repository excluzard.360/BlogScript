/*
    full full-short full-long short long numeric 
    nonPipe: Thu Feb 14 2019 18:33:50 GMT+0700 (Indochina Time) 
    pipe : 14/2/2562 
    pipe 'full' : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 
    pipe 'short' : 14 ก.พ. 2562 
    pipe 'long' : 14 กุมภาพันธ์ 2562 
    pipe 'numeric': 14/2/2562 

    pipe ['full'] : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 
    pipe ['full-long'] : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 
    pipe ['full-short'] : พฤ. 14 ก.พ. 2562 
    pipe ['numeric'] : 14/2/2562 
    pipe ['long'] : 14 กุมภาพันธ์ 2562 
    pipe ['short'] : 14 ก.พ. 2562 

    pipe ['full-short'] : พฤ. 14 ก.พ. 2562 
    pipe ['full-short', 'numeric'] : พฤ. 14/2/2562 
    pipe ['full-short', 'long'] : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 

    pipe ['full', 'numeric'] : วันพฤหัสบดี 14/2/2562 
    pipe ['full', 'long'] : วันพฤหัสบดีที่ 14 กุมภาพันธ์ 2562 
    pipe ['full', 'short'] : พฤ. 14 ก.พ. 2562 

----------- e.g. -------------------
    nonPipe: {{birthDate.value}} <br>
    pipe          : {{ birthDate.value | dateLocalTh }} <br>
    pipe 'full'   : {{ birthDate.value | dateLocalTh:'full' }} <br>
    pipe 'short'  : {{ birthDate.value | dateLocalTh:'short' }} <br>
    pipe 'long'   : {{ birthDate.value | dateLocalTh:'long' }} <br>
    pipe 'numeric': {{ birthDate.value | dateLocalTh:'numeric' }} <br>
<br>
    pipe ['full']       : {{ birthDate.value | dateLocalTh: ['full'] }} <br>
    pipe ['full-long']  : {{ birthDate.value | dateLocalTh: ['full-long'] }} <br>
    pipe ['full-short'] : {{ birthDate.value | dateLocalTh: ['full-short'] }} <br>
    pipe ['numeric']    : {{ birthDate.value | dateLocalTh: ['numeric'] }} <br>
    pipe ['long']       : {{ birthDate.value | dateLocalTh: ['long'] }} <br>
    pipe ['short']      : {{ birthDate.value | dateLocalTh: ['short'] }} <br>
<br>
    pipe ['full-short']     : {{ birthDate.value | dateLocalTh: ['full-short'] }} <br>
    pipe ['full-short', 'numeric']  : {{ birthDate.value | dateLocalTh: ['full-short', 'numeric'] }} <br>
    pipe ['full-short', 'long']     : {{ birthDate.value | dateLocalTh: ['full-short', 'long'] }} <br>
<br>
    pipe ['full', 'numeric']  : {{ birthDate.value | dateLocalTh: ['full', 'numeric'] }} <br>
    pipe ['full', 'long']     : {{ birthDate.value | dateLocalTh: ['full', 'long'] }} <br>
    pipe ['full', 'short']     : {{ birthDate.value | dateLocalTh: ['full', 'short'] }} <br>


  **ps. e.g comboo <br>

  testDate :string = "2019/11/22"; ==> 
  testDate :string = "2562/11/22"; ==>
    {{testDate | toDate:'YYYY/MM/DD' | dateLocalTh: ['numeric'] }} <br>
  output
    22/11/2562 


*/
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateLocalTh'
})
export class DateLocalThPipe implements PipeTransform {

  transform(value: any, args?: any): any {


    console.log("args:", args);
    var options = this.genOptions(args);

      if (value instanceof Date) {
        if(!args || args.length == 0){
          options = { year: 'numeric', month: 'numeric', day: 'numeric' };
        }

        try {
          var date = value;
          return date.toLocaleDateString("th", options);
          
        } catch (error) {
          return value;
        }

      }
    
    return value;
  }

  genOptions(args?: any){
    if (args instanceof Array) {
        
    }else if(args){
      args = [args];
    }else{
      args = [];
    }

    // full full-short  full-long short long numeric
    var options = {year: 'numeric', month: 'numeric', day: 'numeric'};
    
    var weekday = undefined;

    if(args.find((data)=>data=='full') || args.find((data)=>data=='full-long')){
      options['weekday'] =  'long';
      options['month'] =  'long';
      
    }
    if(args.find((data)=>data=='full-short')){
      options['weekday'] =  'short';
      options['month'] =  'short';
    }
    
    if(args.find((data)=>data=='short')){
      options['month'] = 'short';
      weekday = 'short';
    }

    if(args.find((data)=>data=='long')){
      options['month'] = 'long';
      weekday = 'long';
    }

    if(args.find((data)=>data=='numeric')){
      options['month'] = 'numeric';
    }
    
    if(options['weekday'] && weekday){
      options['weekday'] = weekday;
    }

    return options;

  }
    

}
